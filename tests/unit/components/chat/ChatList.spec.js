import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';

import ChatList from '@/components/chat/ChatList.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('AppToolbar.vue', () => {
  let store;

  const mockSetTitle = jest.fn();
  const mockOnChats = jest.fn();

  beforeEach(() => {
    Vue.use(Vuetify);

    store = new Vuex.Store({
      state: {
        notifications: [],
        user: { id: '15', username: 'testuser' },
        chats: {
          '10': {
            hasNew: null,
            userIds: {
              '10': { username: 'testcontact1' },
              '14': { username: 'testcontact2' },
              '15': { username: 'user' },
            },
            from: '15',
            lastMessage: 'mocktext',
          },
        },
        contacts: {
          '10': { username: 'testcontact' },
          '14': { username: 'testcontact2' },
        },
      },
      getters: {
        user: (state) => state.user,
        chats: (state) => state.chats,
        contacts: (state) => state.contacts,
        notifications: (state) => state.notifications,
      },
      actions: {
        setTitle: mockSetTitle,
        onChats: mockOnChats,
      },
    });
  });

  afterEach(() => {
    store = null;
  });

  it("Should nav to messages, and set chat 'hasNew' to false", () => {
    const MOCK_USERNAMES = ['user1', 'user2'];
    const push = jest.fn();
    const mapToUsernames = jest.fn().mockReturnValue(MOCK_USERNAMES);

    const $router = { push };
    const mockId = '10';

    const wrapper = shallowMount(ChatList, {
      localVue,
      store,
      mocks: { $router },
      methods: {
        mapToUsernames,
        mapToFromText: jest.fn(),
        mapToDate: jest.fn(),
      },
    });

    wrapper.vm.navToMessages(mockId);

    expect(wrapper.vm.chats[mockId].hasNew).toBe(false);
    expect(push).toHaveBeenCalledWith({
      name: 'messages',
      params: { id: mockId, usernames: MOCK_USERNAMES },
    });
  });

  it('Should map chat to usernames', () => {
    const mockChat = {
      userIds: {
        '10': { username: 'testcontact1' },
        '14': { username: 'testcontact2' },
        '15': { username: 'user' },
      },
    };

    const wrapper = shallowMount(ChatList, {
      localVue,
      store,
      methods: {
        mapToFromText: jest.fn(),
        mapToDate: jest.fn(),
      },
    });

    const usernames = wrapper.vm.mapToUsernames(mockChat);

    expect(usernames).toBe('testcontact,testcontact2');
  });

  it('Should map chat to from text', () => {
    const mockChat = {
      hasNew: null,
      userIds: {
        '10': true,
        '14': true,
        '15': true,
      },
      from: '15',
      lastMessage: 'mocktext',
    };

    const wrapper = shallowMount(ChatList, {
      localVue,
      store,
      methods: {
        mapToUsernames: jest.fn(),
        mapToDate: jest.fn(),
      },
    });

    const text = wrapper.vm.mapToFromText(mockChat);

    expect(text).toBe('You: mocktext');
  });

  it('Should map chat to from text', () => {
    const mockChat = {
      hasNew: null,
      userIds: {
        '10': { username: 'testcontact1' },
        '14': { username: 'testcontact2' },
        '15': { username: 'user' },
      },
      from: '14',
      lastMessage: 'mocktext',
    };

    const wrapper = shallowMount(ChatList, {
      localVue,
      store,
      methods: {
        mapToUsernames: jest.fn(),
        mapToDate: jest.fn(),
      },
    });

    const text = wrapper.vm.mapToFromText(mockChat);

    expect(text).toBe('testcontact2: mocktext');
  });
});
