import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';

import NewChatDialog from '@/components/chat/NewChatDialog.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('AppToolbar.vue', () => {
  let store;

  const getActiveChat = jest.fn().mockReturnValue(Promise.resolve('activeId'));
  const createChat = jest.fn().mockReturnValue(Promise.resolve('newId'));
  const sendMessage = jest.fn().mockReturnValue(Promise.resolve());

  beforeEach(() => {
    Vue.use(Vuetify);

    store = new Vuex.Store({
      state: {
        user: { id: 'userId' },
        chats: {},
        contacts: { mock: { username: 'test' } },
      },
      getters: {
        user: (state) => state.user,
        chats: (state) => state.chats,
        contacts: (state) => state.contacts,
      },
      actions: { getActiveChat, createChat, sendMessage },
    });
  });

  afterEach(() => {
    store = null;
  });

  it('Should send message to active chat', () => {
    const push = jest.fn();
    const $router = { push };

    const wrapper = shallowMount(NewChatDialog, {
      localVue,
      store,
      data() {
        return {
          select: [{ id: 'user1' }, { id: 'user2' }],
        };
      },
      props: { contactId: 'mock' },
      methods: { created: jest.fn() },
      stubs: ['v-combobox'],
      mocks: { $router },
    });

    wrapper.vm.sendMessage().then(() => {
      expect(getActiveChat).toHaveBeenCalled();
      expect(createChat).not.toHaveBeenCalled();
      expect(sendMessage).toHaveBeenCalled();
      expect(push).toHaveBeenCalledWith({
        name: 'messages',
        params: { id: 'activeId' },
      });
    });
  });

  it('Should send message to active chat', () => {
    const wrapper = shallowMount(NewChatDialog, {
      localVue,
      store,
      propsData: { contactId: 'mock' },
      stubs: ['v-combobox'],
    });

    const EXPECTED_USER = { username: 'test' };
    expect(wrapper.vm.select).toEqual([EXPECTED_USER]);
  });
});
