import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import routes from '@/routes';

import App from '@/components/app/App.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

describe('App.vue', () => {
  let vuetify;
  let router;
  let store;

  const mockLogout = jest.fn();
  const mockReset = jest.fn();
  const mockInitApp = jest.fn();

  const mockLocalStorage = {
    data: {
      vuex: { test: 'mock' },
    },
    removeItem(key) {
      delete this.data[key];
    },
  };

  beforeEach(() => {
    Vue.use(Vuetify);
    store = new Vuex.Store({
      state: {
        user: {},
        isBusy: false,
        notifications: [],
      },
      getters: {
        notifications: (state) => state.notifications,
        user: (state) => state.user,
        isBusy: (state) => state.isBusy,
      },
      actions: {
        logout: mockLogout,
        reset: mockReset,
        initializeApp: mockInitApp,
      },
    });
    vuetify = new Vuetify();
    router = new VueRouter({ routes });
  });

  it('Should trigger successful logout', () => {
    const clearUserReferences = jest.fn();

    const wrapper = shallowMount(App, {
      store,
      localVue,
      router,
      vuetify,
      methods: { clearUserReferences },
    });
    wrapper.vm.drawerOpen = true;

    expect(mockLocalStorage.data).toEqual({ vuex: { test: 'mock' } });

    return wrapper.vm.onLogout(mockLocalStorage).then(() => {
      expect(mockLogout).toHaveBeenCalled();
      expect(clearUserReferences).toHaveBeenCalled();
      expect(mockReset).toHaveBeenCalled();
      expect(mockInitApp).toHaveBeenCalled();
      expect(wrapper.vm.drawerOpen).toBe(false);
      expect(mockLocalStorage.data).toEqual({});
    });
  });

  it('Should clear user references', () => {
    const mockOff = jest.fn();
    const wrapper = shallowMount(App, {
      store,
      localVue,
      router,
      vuetify,
      data() {
        return {
          contactRefs: [
            { contact: 'ref', off: mockOff },
            { contact: 'ref', off: mockOff },
          ],
          notifRef: { notif: 'ref', off: mockOff },
        };
      },
    });

    wrapper.vm.clearUserReferences();

    expect(wrapper.vm.contactRefs).toEqual([]);
    expect(mockOff).toHaveBeenCalledTimes(3);
  });
});
