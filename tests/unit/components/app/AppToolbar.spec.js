import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';

import AppToolbar from '@/components/app/AppToolbar.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('AppToolbar.vue', () => {
  let store;

  beforeEach(() => {
    Vue.use(Vuetify);

    store = new Vuex.Store({
      state: {
        title: 'mockTitle',
        subtitle: 'mockSubtitle',
      },
      getters: {
        title: (state) => state.title,
        subtitle: (state) => state.subtitle,
      },
      actions: {},
    });
  });

  afterEach(() => {
    store = null;
  });

  it('Should nav back', () => {
    const $route = {
      name: 'contacts',
      meta: {
        toolbar: {
          enable: true,
          back: { enable: true, route: 'chats' },
          logout: { enable: true },
        },
      },
    };
    const push = jest.fn();
    const $router = { push };

    const wrapper = shallowMount(AppToolbar, {
      localVue,
      store,
      mocks: { $route, $router },
    });

    wrapper.vm.back();

    expect(push).toHaveBeenCalledWith({ name: 'chats' });
  });
});
