import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount } from '@vue/test-utils';

import AppNavDrawer from '@/components/app/navdrawer/AppNavDrawer.vue';

describe('AppToolbar.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify);
  });

  afterEach(() => {});

  it('Should not navigate but should still emit close', () => {
    const $route = { name: 'contacts' };
    const push = jest.fn();
    const $router = { push };

    const wrapper = shallowMount(AppNavDrawer, { mocks: { $route, $router } });

    wrapper.vm.navTo('contacts');

    expect(push).not.toHaveBeenCalled();
    expect(wrapper.emitted().close);
  });

  it('Should navigate to mock, and should emit close', () => {
    const $route = { name: 'test' };
    const push = jest.fn();
    const $router = { push };

    const wrapper = shallowMount(AppNavDrawer, { mocks: { $route, $router } });

    wrapper.vm.navTo('mock');

    expect(push).toHaveBeenCalledWith({ name: 'mock' });
    expect(wrapper.emitted().close);
  });
});
