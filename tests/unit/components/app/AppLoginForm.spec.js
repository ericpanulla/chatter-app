import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import routes from '@/routes';

import AppLoginForm from '@/components/app/AppLoginForm.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

describe('AppLoginForm.vue', () => {
  let router;
  let store;

  let mockLogin;
  let mockOnUser;
  let mockSetTitle;
  let mockCreateAccount;

  beforeEach(() => {
    Vue.use(Vuetify);

    mockLogin = jest.fn();
    mockOnUser = jest.fn();
    mockSetTitle = jest.fn();
    mockCreateAccount = jest.fn();

    store = new Vuex.Store({
      state: {},
      getters: {},
      actions: {
        login: mockLogin,
        onUser: mockOnUser,
        setTitle: mockSetTitle,
        createAccount: mockCreateAccount,
      },
    });
    router = new VueRouter({ routes });
  });

  afterEach(() => {
    mockLogin = null;
    mockOnUser = null;
    mockSetTitle = null;
    mockCreateAccount = null;

    store = null;
    router = null;
  });

  it('Should set title on mount', () => {
    shallowMount(AppLoginForm, {
      store,
      localVue,
      router,
    });

    expect(mockSetTitle).toHaveBeenCalled();
  });

  it('Should submit successfully', () => {
    const wrapper = shallowMount(AppLoginForm, {
      store,
      localVue,
      router,
    });

    return wrapper.vm.submit().then(() => {
      expect(mockLogin).toHaveBeenCalled();
      expect(mockOnUser).toHaveBeenCalled();
    });
  });

  it('Should NOT submit successfully', () => {
    mockLogin = jest.fn().mockRejectedValue(new Error('mock login error'));

    const mockStore = new Vuex.Store({
      state: {},
      getters: {},
      actions: {
        login: mockLogin,
        onUser: mockOnUser,
        setTitle: mockSetTitle,
      },
    });
    const wrapper = shallowMount(AppLoginForm, {
      store: mockStore,
      localVue,
      router,
    });

    return wrapper.vm.submit().catch((err) => {
      expect(mockLogin).toHaveBeenCalled();
      expect(mockOnUser).not.toHaveBeenCalled();
      expect(err.message).toBe('mock login error');
    });
  });

  it('Should create account successfully', () => {
    const wrapper = shallowMount(AppLoginForm, {
      store,
      localVue,
      router,
    });

    return wrapper.vm.onRegister().then(() => {
      expect(mockCreateAccount).toHaveBeenCalled();
      expect(mockOnUser).toHaveBeenCalled();
    });
  });

  it('Should NOT create account successfully', () => {
    mockCreateAccount = jest
      .fn()
      .mockRejectedValue(new Error('mock create account error'));

    const mockStore = new Vuex.Store({
      state: {},
      getters: {},
      actions: {
        login: mockLogin,
        onUser: mockOnUser,
        setTitle: mockSetTitle,
        createAccount: mockCreateAccount,
      },
    });

    const wrapper = shallowMount(AppLoginForm, {
      store: mockStore,
      localVue,
      router,
    });

    return wrapper.vm.onRegister().catch((err) => {
      expect(mockCreateAccount).toHaveBeenCalled();
      expect(mockOnUser).not.toHaveBeenCalled();
      expect(err.message).toBe('mock create account error');
    });
  });
});
