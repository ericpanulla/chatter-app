import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Vue from 'vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';

import NewContactDialog from '@/components/contacts/NewContactDialog.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('AppToolbar.vue', () => {
  let store;

  const requestContact = jest.fn();
  const getUsers = jest.fn();

  beforeEach(() => {
    Vue.use(Vuetify);

    store = new Vuex.Store({
      state: {},
      getters: {},
      actions: { requestContact, getUsers },
    });
  });

  afterEach(() => {
    store = null;
  });

  it('Should submit request', () => {
    const wrapper = shallowMount(NewContactDialog, {
      localVue,
      store,
      methods: { mounted: jest.fn() },
      data() {
        return { select: 'mockId' };
      },
    });

    wrapper.vm.submitRequest().then(() => {
      expect(requestContact).toHaveBeenCalled();
    });
  });
});
