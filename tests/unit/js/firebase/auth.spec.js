import {
  getUser,
  loginWithEmail,
  logout,
  registerWithEmail,
} from '@/js/firebase/auth';

describe('auth.js', () => {
  let mockAuth, mockApp, auth, mockUnsubscribe;

  beforeEach(() => {
    mockUnsubscribe = jest.fn();
    mockAuth = {
      cb: jest.fn(),
      onAuthStateChanged(cb) {
        this.cb = cb;
        return mockUnsubscribe;
      },
      fireAuthStateChange(user) {
        this.cb(user);
      },
    };
    auth = jest.fn().mockReturnValue(mockAuth);
    mockApp = { auth };
  });

  it('Should resolve user, user was logged in', () => {
    const mockUser = {
      id: 'mockId',
      username: 'mockUsername',
      email: 'mockEmail',
    };

    // register listener on promise
    const test = getUser(mockApp).then((user) => {
      expect(mockUnsubscribe).toHaveBeenCalled();
      expect(user).not.toBeNull();
      expect(user).toBeDefined();
      expect(user).toEqual(mockUser);
    });

    // fire event to trigger test
    mockAuth.fireAuthStateChange(mockUser);
    return test;
  });

  it('Should resolve null, user was logged out', () => {
    const mockUser = null;

    // register listener on promise
    getUser(mockApp).then((user) => {
      expect(mockUnsubscribe).toHaveBeenCalled();
      expect(user).toBeNull();
      expect(user).toBeDefined();
      expect(user).toEqual(mockUser);
    });

    // fire event to trigger test
    mockAuth.fireAuthStateChange(mockUser);
  });

  it('Should reject invalid app', () => {
    const mockUser = {};

    // register listener on promise
    const test = getUser({}).catch((err) => {
      expect(err.message).toBe('Firebase auth has not been initialized.');
    });

    // fire event to trigger test
    mockAuth.fireAuthStateChange(mockUser);
    return test;
  });

  it('Should resolve user, valid email and password, already logged in', () => {
    const mockUser = {
      id: 'mockId',
      username: 'mockUsername',
      email: 'mockEmail',
    };
    const mockEmail = 'mockEmail';
    const mockPass = 'mockPass';
    const mockSignInEmail = () => mockAuth.fireAuthStateChange(mockUser);

    mockAuth.signInWithEmailAndPassword = mockSignInEmail;

    // if valid email and password, but already logged in should return user
    // if valid email and password, should log in and return user
    return loginWithEmail(mockApp, mockEmail, mockPass).then((user) => {
      expect(mockUnsubscribe).toHaveBeenCalled();
      expect(user).not.toBeNull();
      expect(user).toBeDefined();
      expect(user).toEqual(mockUser);
    });
  });

  it('Should reject, invalid email and password', () => {
    const mockEmail = 'mockEmail';
    const mockPass = 'mockPass';

    const mockError = new Error('Invalid email or password');
    const mockSignInEmail = () => {
      throw mockError;
    };

    mockAuth.signInWithEmailAndPassword = mockSignInEmail;

    // if invalid email and password, should not log in and reject
    return loginWithEmail(mockApp, mockEmail, mockPass).catch((err) => {
      expect(err.message).toBe('Invalid email or password');
    });
  });

  it('Should reject, invalid app', () => {
    const mockEmail = 'mockEmail';
    const mockPass = 'mockPass';

    const mockError = new Error('Invalid email or password');
    const mockSignInEmail = () => {
      throw mockError;
    };

    mockAuth.signInWithEmailAndPassword = mockSignInEmail;

    // if invalid email and password, should not log in and reject
    return loginWithEmail({}, mockEmail, mockPass).catch((err) => {
      expect(err.message).toBe('Firebase auth has not been initialized.');
    });
  });

  it('Should logout, resolve', () => {
    const mockUser = null;
    const mockSignOut = () => {
      return new Promise((resolve, reject) => {
        mockAuth.fireAuthStateChange(mockUser);
        resolve();
      });
    };

    mockAuth.signOut = mockSignOut;

    return logout(mockApp).then((val) => {
      expect(val).not.toBeDefined();
    });
  });

  it('Should not logout, reject', () => {
    const mockError = new Error('Already logged in');
    const mockSignOut = () => {
      throw mockError;
    };
    mockAuth.signOut = mockSignOut;

    logout(mockApp).catch((err) => {
      expect(err.message).toBe('Already logged in');
    });
  });

  it('Should reject, invalid app', () => {
    const mockError = new Error('Already logged in');
    const mockSignOut = () => {
      throw mockError;
    };
    mockAuth.signOut = mockSignOut;

    logout({}).catch((err) => {
      expect(err.message).toBe('Firebase auth has not been initialized.');
    });
  });

  it('Should register successfully', () => {
    const mockRegister = jest.fn().mockResolvedValue({
      user: {
        id: 'mockId',
        username: 'mockUsername',
        email: 'mockEmail',
      },
    });

    mockAuth.createUserWithEmailAndPassword = mockRegister;

    registerWithEmail('mockEmail', 'mockPassword').then((user) => {
      expect(user).toEqual({
        id: 'mockId',
        username: 'mockUsername',
        email: 'mockEmail',
      });
    });
  });

  it('Should not register, throws error', () => {
    const mockError = new Error('cant register');
    const mockRegister = jest.fn().mockRejectedValue(mockError);

    mockAuth.createUserWithEmailAndPassword = mockRegister;

    registerWithEmail(mockApp, 'mockEmail', 'mockPassword').catch((err) => {
      expect(err.message).toBe('cant register');
    });
  });

  it('Should reject, invalid app', () => {
    const mockError = new Error('cant register');
    const mockRegister = jest.fn().mockRejectedValue(mockError);

    mockAuth.createUserWithEmailAndPassword = mockRegister;

    registerWithEmail({}, 'mockEmail', 'mockPassword').catch((err) => {
      expect(err.message).toBe('Firebase auth has not been initialized.');
    });
  });
});
