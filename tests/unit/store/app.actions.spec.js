import * as init from '@/js/firebase/initialize';
import actions from '@/store/modules/app/app.actions';

import { app } from '../../firebaseMocks';

describe('app.actions.js', () => {
  beforeEach(() => {
    jest.mock('@/js/firebase/initialize');
    location.reload = jest.fn();
  });

  afterEach(() => {});

  it('Should initialize app successfully', async () => {
    const mockCommit = jest.fn();
    const mockApp = app();

    init.initApp = jest.fn().mockResolvedValue(mockApp);

    const resultingApp = await actions.initializeApp({ commit: mockCommit });

    expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
    expect(mockCommit).toHaveBeenCalledWith('app', mockApp);
    expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
    expect(resultingApp).toEqual(mockApp);
  });

  it('Should NOT initialize app successfully', async () => {
    const mockCommit = jest.fn();
    const mockApp = app();

    init.initApp = jest.fn().mockRejectedValue();

    try {
      await actions.initializeApp({ commit: mockCommit });
    } catch (err) {
      expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
      expect(mockCommit).not.toHaveBeenCalledWith('app', mockApp);
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
    }
  });

  it('Should set on user callback', async () => {
    const mockCommit = jest.fn();
    const mockData = { username: 'mockUsername', id: 'mockId' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    const resultingRef = actions.onUser(
      { commit: mockCommit, getters: mockGetters },
      'mockId'
    );

    expect(mockCommit).toHaveBeenCalledWith('user', {
      id: 'mockKey',
      ...{ username: 'mockUsername', id: 'mockId' },
    });
    expect(resultingRef).toEqual(mockApp.database());
  });

  it('Should NOT set on user callback', async () => {
    const mockCommit = jest.fn();
    const mockData = null;
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    const resultingRef = actions.onUser(
      { commit: mockCommit, getters: mockGetters },
      'mockId'
    );

    expect(mockCommit).not.toHaveBeenCalled();
    expect(resultingRef).toEqual(mockApp.database());
  });

  it('Should get users', async () => {
    const mockData = {
      id1: { username: 'mockUser1' },
      id2: { username: 'mockUser2' },
    };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    const resultingUsers = await actions.getUsers({ getters: mockGetters });

    expect(resultingUsers).toEqual([
      { id: 'id1', ...mockData['id1'] },
      { id: 'id2', ...mockData['id2'] },
    ]);
  });
});
