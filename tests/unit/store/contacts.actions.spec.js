import * as utils from '@/js/firebase/utils';
import actions from '@/store/modules/contacts/contacts.actions';

import { app } from '../../firebaseMocks';

describe('contacts.actions.js', () => {
  beforeEach(() => {
    jest.mock('@/js/firebase/utils');
  });

  it('Should register on contacts and commit', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'contact' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    actions.onContacts({ commit: mockCommit, getters: mockGetters }, 'mockId');

    expect(mockApp.database().on).toHaveBeenCalledTimes(2);
    expect(mockCommit).toHaveBeenCalledWith('contact', {
      id: 'mockKey',
      data: mockData,
    });
  });

  it('Should request contact', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'contact' };
    const mockApp = app(mockData);
    const mockGetters = {
      app: mockApp,
      user: { id: 'mockId', username: 'mockUsername', avatar: 'mockAvatar' },
      contacts: {
        id1: { username: 'mockUsername1' },
        id2: { username: 'mockUsername2' },
      },
    };

    actions.requestContact(
      { commit: mockCommit, getters: mockGetters },
      'mockId'
    );

    expect(mockApp.database().push).toHaveBeenCalled();
    expect(mockApp.database().push().set).toHaveBeenCalledWith({
      type: 'contact',
      data: {
        from: {
          id: 'mockId',
          username: 'mockUsername',
          avatar: 'mockAvatar',
        },
      },
    });
    expect(mockCommit).not.toHaveBeenCalled();
  });

  it('Should request contact', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'contact' };
    const mockApp = app(mockData);
    const mockGetters = {
      app: mockApp,
      user: { id: 'mockId', username: 'mockUsername', avatar: 'mockAvatar' },
      contacts: {
        id1: { username: 'mockUsername1' },
        id2: { username: 'mockUsername2' },
      },
    };

    actions.requestContact({ commit: mockCommit, getters: mockGetters }, 'id1');

    expect(mockApp.database().push).not.toHaveBeenCalled();
    expect(mockApp.database().push().set).not.toHaveBeenCalledWith();
    expect(mockCommit).toHaveBeenCalledWith('errorMessage', {
      title: 'Requested user is already a contact!',
      text: 'Please try a different user',
      timeout: 5000,
    });
  });
});
