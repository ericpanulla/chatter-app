import * as utils from '@/js/firebase/utils';
import actions from '@/store/modules/notifications/notifications.actions';

import { app } from '../../firebaseMocks';

describe('notifications.actions.js', () => {
  beforeEach(() => {
    jest.mock('@/js/firebase/utils');
  });

  it('Should register on notifications and commit', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'contact' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    const resultRef = actions.onNotifications(
      { commit: mockCommit, getters: mockGetters },
      'mockId'
    );

    expect(mockApp.database().on).toHaveBeenCalledTimes(2);
    expect(mockCommit).toHaveBeenCalledWith('notification', {
      id: 'mockKey',
      ...mockData,
    });

    setTimeout(() => {
      expect(mockCommit).toHaveBeenCalledWith('removeNotification', {
        id: 'mockKey',
      });
    }, 5000);

    expect(resultRef).toEqual(mockApp.database());
  });
});
