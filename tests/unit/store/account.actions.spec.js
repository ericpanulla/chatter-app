import * as auth from '@/js/firebase/auth';
import actions from '@/store/modules/account/account.actions';

import { app } from '../../firebaseMocks';

describe('account.actions.js', () => {
  beforeEach(() => {
    jest.mock('@/js/firebase/auth');
  });

  afterEach(() => {});

  it('Should set user auth successfully', async () => {
    const mockCommit = jest.fn();
    const mockApp = app();
    const mockGetters = { app: mockApp };

    auth.getUser = jest.fn().mockResolvedValue({ username: 'mock' });

    await actions.setUserAuth({ commit: mockCommit, getters: mockGetters });

    expect(auth.getUser).toHaveBeenCalled();
    expect(mockCommit).toHaveBeenCalledWith('authState', true);
  });

  it('Should login successfully', async () => {
    const mockEmail = 'mockEmail';
    const mockPassword = 'mockPassword';

    const mockCommit = jest.fn();
    const mockData = { username: 'mockUser' };

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    mockApp.auth().signInWithEmailAndPassword = () =>
      mockApp.auth().fireAuthStateChange({ username: 'mockUser' });

    let userKey;
    try {
      userKey = await actions.login(
        { commit: mockCommit, getters: mockGetters },
        { email: mockEmail, password: mockPassword }
      );

      expect(mockGetters.app.database().update).toHaveBeenCalledWith({
        isActive: true,
      });
      expect(mockGetters.app.database().once).toHaveBeenCalled();
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
      expect(mockCommit).toHaveBeenCalledWith('user', mockData);
      expect(userKey).toBe('mockKey');
      // eslint-disable-next-line no-empty
    } catch (err) {}
  });

  it('Should NOT login successfully', async () => {
    const mockEmail = 'mockEmail';
    const mockPassword = 'mockPassword';

    const mockCommit = jest.fn();
    const mockData = null;

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    mockApp.auth().signInWithEmailAndPassword = () =>
      mockApp.auth().fireAuthStateChange({ username: 'mockUser' });

    try {
      await actions.login(
        { commit: mockCommit, getters: mockGetters },
        { email: mockEmail, password: mockPassword }
      );
    } catch (err) {
      expect(mockGetters.app.database().update).toHaveBeenCalledWith({
        isActive: true,
      });
      expect(mockGetters.app.database().once).toHaveBeenCalled();
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
      expect(mockCommit).toHaveBeenCalledWith('errorMessage', {
        title: 'Unable to login!',
        text: 'Please check your username or password.',
        timeout: 5000,
      });
      expect(err.message).toBe('User not found in database');
    }
  });

  it('Should logout successfully', async () => {
    const mockCommit = jest.fn();
    const mockData = { username: 'mockUser' };

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp, user: { id: 'mockId' } };

    auth.logout = jest.fn().mockResolvedValue();

    await actions.logout({ commit: mockCommit, getters: mockGetters });

    expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
    expect(mockApp.database().update).toHaveBeenCalledWith({ isActive: false });
    expect(mockCommit).toHaveBeenCalledWith('user', null);
    expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
  });

  it('Should NOT logout successfully', async () => {
    const mockCommit = jest.fn();
    const mockData = { username: 'mockUser' };

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp, user: { id: 'mockId' } };

    auth.logout = jest.fn().mockRejectedValue();

    try {
      await actions.logout({ commit: mockCommit, getters: mockGetters });
    } catch (err) {
      expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
      expect(mockApp.database().update).not.toHaveBeenCalled();
      expect(mockCommit).not.toHaveBeenCalledWith('user', null);
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
    }
  });

  it('Should create account successfully', async () => {
    const mockCommit = jest.fn();
    const mockData = { username: 'mockUser' };

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };
    const mockForm = {
      username: 'mockUsername',
      email: 'mockEmail',
      password: 'mocKPassword',
      avatar: 'mockAvatar',
    };

    auth.registerWithEmail = jest.fn().mockResolvedValue({ uid: 'mockUid' });

    let uid;
    try {
      uid = actions.createAccount(
        { commit: mockCommit, getters: mockGetters },
        mockForm
      );
    } catch (err) {
      expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
      expect(mockApp.database().push().set).toHaveBeenCalledWith({
        username: 'mockUsername',
        email: 'mockEmail',
        avatar: 'mockAvatar',
        isActive: true,
      });
      expect(uid).toBe('mockUid');
    }
  });

  it('Should NOT create account successfully', async () => {
    const mockCommit = jest.fn();
    const mockData = { username: 'mockUser' };

    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };
    const mockForm = {
      username: 'mockUsername',
      email: 'mockEmail',
      password: 'mocKPassword',
      avatar: 'mockAvatar',
    };

    auth.registerWithEmail = jest.fn().mockRejectedValue();

    try {
      actions.createAccount(
        { commit: mockCommit, getters: mockGetters },
        mockForm
      );
    } catch (err) {
      expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
      expect(mockCommit).toHaveBeenCalledWith('errorMessage', {
        title: 'Unable to create new account.',
        text: err,
        timeout: 5000,
      });
    }
  });
});
