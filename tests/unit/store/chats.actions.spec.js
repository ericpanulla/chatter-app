import * as utils from '@/js/firebase/utils';
import actions from '@/store/modules/chats/chats.actions';

import { app } from '../../firebaseMocks';

describe('chats.actions.js', () => {
  beforeEach(() => {
    jest.mock('@/js/firebase/utils');
  });

  afterEach(() => {});

  it('Should register on messages and commit', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'message' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    actions.onMessages({ commit: mockCommit, getters: mockGetters }, 'mockId');

    expect(mockApp.database().on).toHaveBeenCalled();
    expect(mockCommit).toHaveBeenCalledWith('message', {
      chatId: 'mockId',
      id: 'mockKey',
      data: mockData,
    });
  });

  it('Should register on chats and commit', async () => {
    const mockCommit = jest.fn();
    const mockData = { mock: 'message' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    actions.onChats({ commit: mockCommit, getters: mockGetters }, 'mockId');

    expect(mockApp.database().on).toHaveBeenCalledTimes(2);
    expect(mockCommit).toHaveBeenCalledWith('chat', {
      id: 'mockKey',
      data: mockData,
    });
  });

  it('Should send message', async () => {
    const mockMessage = { text: 'mockText', from: 'mockFrom' };
    const mockData = { mock: 'message' };
    const mockApp = app(mockData);
    const mockGetters = { app: mockApp };

    actions.sendMessage(
      { getters: mockGetters },
      { chatId: 'mockId', newMessage: mockMessage }
    );

    expect(mockApp.database().push).toHaveBeenCalled();
    expect(mockApp.database().push().set).toHaveBeenCalledWith(mockMessage);
  });

  it('Should get active chat', async () => {
    const mockIds = ['id1', 'id2'];
    const mockChats = { mockKey: { userIds: { id1: true, id2: true } } };
    const mockGetters = { chats: mockChats };

    let key;
    try {
      key = await actions.getActiveChat({ getters: mockGetters }, mockIds);
      // eslint-disable-next-line no-empty
    } catch (err) {}

    expect(key).toBe('mockKey');
  });

  it('Should NOT get active chat', async () => {
    const mockIds = ['id1', 'id3'];
    const mockChats = { mockKey: { userIds: { id1: true, id2: true } } };
    const mockGetters = { chats: mockChats };

    let key;
    try {
      key = await actions.getActiveChat({ getters: mockGetters }, mockIds);
      // eslint-disable-next-line no-empty
    } catch (err) {
      expect(key).not.toBe('mockKey');
    }
  });

  it('Should create chat', async () => {
    const mockCommit = jest.fn();
    const mockApp = app();
    const mockGetters = { app: mockApp };
    const mockIds = ['id1', 'id2'];

    let key;
    try {
      key = actions.createChat(
        { commit: mockCommit, getters: mockGetters },
        mockIds
      );

      expect(mockCommit).toHaveBeenCalledWith('isBusy', true);
      expect(mockApp.database().push).toHaveBeenCalled();
      expect(mockApp.database().push().set).toHaveBeenCalledWith({
        userIds: { id1: true, id2: true },
      });
      expect(mockApp.database().push().set).toHaveBeenCalledWith(true);
      expect(mockCommit).toHaveBeenCalledWith('isBusy', false);
      expect(key).toBe('mockKey');
      // eslint-disable-next-line no-empty
    } catch (err) {}
  });
});
