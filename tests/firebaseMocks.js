/* eslint-disable no-undef */
const snapshot = (mockData) => {
  return { val: () => mockData, key: 'mockKey' };
};

const auth = () => {
  return {
    cb: jest.fn(),
    onAuthStateChanged(cb) {
      this.cb = cb;
      return jest.fn();
    },
    fireAuthStateChange(user) {
      this.cb(user);
    },
  };
};

export const app = (mockData) => {
  return {
    auth: jest.fn().mockReturnValue(auth()),
    database: jest.fn().mockReturnValue({
      ref: jest.fn().mockReturnThis(),
      child: jest.fn().mockReturnThis(),
      once: jest.fn().mockImplementation((type, cb) => cb(snapshot(mockData))),
      on: jest.fn().mockImplementation((type, cb) => cb(snapshot(mockData))),
      update: jest.fn(),
      orderByChild: jest.fn().mockReturnThis(),
      push: jest.fn().mockReturnValue({
        key: 'mockKey',
        set: jest.fn(),
      }),
    }),
  };
};
