# chatter-app

Ch@tter is a simple messaging app that allows users to communicate freely over the web. It has most features users expect such as:

User profiles
Group messaging
Friend requests
Contact List
Notifications (messages and friend requests)
Color coded chats
Supports multiple open chats
Mobile-friendly
Hosted on Firebase, it takes advantage of the Firebase real-time database to synchronize messages and data across clients almost instantly. The real-time database is a NoSQL, schema-less database providing flexibility and scalability.

On the front-end Ch@tter takes advantage of the framework Vue. Combined with Vuetify, (Google’s Material UI for Vue), the app provides a light-weight, robust user interface.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run unit tests
```
yarn run test:unit
```

## Run live app 

https://chatter-app-f0e5b.firebaseapp.com/#/login


## To-do

- [ ] Finish unit tests, including Firebase Database calls
- [ ] Fix Desktop sizing
- [ ] Implement RxFire https://firebase.googleblog.com/2018/09/introducing-rxfire-easy-async-firebase.html
- [ ] Dates in UTC
- [ ] Various bug fixes



