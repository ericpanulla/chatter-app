import { mapOnChildAdded } from '../../../js/firebase/utils';

export default {
  /**
   * Registers a Firebase database reference listener for messages by chat id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} id The chat id.
   * @returns {Object} The Firebase database reference.
   */
  onMessages({ commit, getters }, id) {
    const db = getters.app.database();
    const ref = db.ref(`messages/${id}`).orderByChild('timestamp');
    ref.on('child_added', (child) => {
      commit('message', { chatId: id, id: child.key, data: child.val() });
    });

    return ref;
  },
  /**
   * Registers a Firebase database reference listener for chats for a given user id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} id The user id.
   * @returns {Array} An array of Firebase database references.
   */
  onChats({ commit, getters }, id) {
    return mapOnChildAdded(getters.app.database(), commit, {
      parentId: id,
      parentPath: 'user-chats',
      childPath: 'chats-meta',
      commitPath: 'chat',
    });
  },
  /**
   * Sends a new message to the given chat id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {Object} messageParams The message details.
   * @param {Object} messageParams.chadId The id of the chat.
   * @param {Object} messageParams.newMessage The message.
   */
  sendMessage({ getters }, { chatId, newMessage }) {
    const db = getters.app.database();
    db.ref(`messages/${chatId}`)
      .push()
      .set(newMessage);
  },
  /**
   * Retrieves the key of an active chat by user ids.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {Array} userIds The user ids of the active chat.
   * @returns {String} The active chat id.
   */
  getActiveChat({ getters }, userIds) {
    const userStr = userIds.sort().join();

    return new Promise((resolve, reject) => {
      Object.keys(getters.chats).forEach((key) => {
        const chat = getters.chats[key];
        const userIdStr = Object.keys(chat.userIds)
          .sort()
          .join();
        if (userIdStr === userStr) {
          resolve(key);
          return;
        }
      });
      reject();
    });
  },
  /**
   * Creates a new chat.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {Array} userIds An array of user ids.
   * @returns {Promise} A promise resolving to the new chat id.
   */
  createChat({ commit, getters }, userIds) {
    commit('isBusy', true);

    const ids = {};
    userIds.sort().forEach((id) => {
      ids[id] = true;
    });

    const db = getters.app.database();
    return new Promise((resolve) => {
      // create chat-meta
      const newChat = db.ref(`chats-meta`).push();
      newChat.set({ userIds: ids });

      // // add user-chats
      userIds.forEach((id) => {
        db.ref(`user-chats/${id}/${newChat.key}`).set(true);
      });

      commit('isBusy', false);
      resolve(newChat.key);
    });
  },
};
