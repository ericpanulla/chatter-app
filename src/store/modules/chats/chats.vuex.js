import actions from './chats.actions';

const initialState = () => {
  return { chats: {}, messages: {} };
};

export default {
  state: initialState(),
  mutations: {
    initialState(state) {
      Object.assign(state, initialState());
    },
    chat(state, { id, data }) {
      if (data) {
        state.chats = { ...state.chats, [id]: data };
      } else {
        delete state.chats[id];
      }
    },
    message(state, { chatId, id, data }) {
      const messages = { ...state.messages };
      if (!messages[chatId]) {
        messages[chatId] = {};
      }
      messages[chatId][id] = data;
      state.messages = messages;
    },
  },
  getters: {
    chats(state) {
      return state.chats;
    },
    messages(state) {
      return state.messages;
    },
  },
  actions,
};
