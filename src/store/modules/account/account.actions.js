import {
  getUser,
  loginWithEmail,
  logout,
  registerWithEmail,
} from '../../../js/firebase/auth';

export default {
  /**
   * Sets the local user auth state.
   * @param {Object} vuexParams The provided Vuex params.
   */
  setUserAuth({ commit, getters }) {
    return getUser(getters.app).then((user) => {
      commit('authState', user !== null);
    });
  },
  /**
   * Logs into the Firebase Auth module.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {Object} loginData The email and password to log in.
   * @returns {Promise} A promise resolving to the user id, or else rejecting with error.
   */
  login({ commit, getters }, { email, password }) {
    return new Promise((resolve, reject) => {
      commit('isBusy', true);

      loginWithEmail(getters.app, email, password).then((registration) => {
        const userRef = getters.app.database().ref(`users/${registration.uid}`);
        userRef.update({ isActive: true });
        userRef.once('value', (snap) => {
          commit('isBusy', false);
          const user = snap.val();
          if (user) {
            commit('user', user);
            resolve(snap.key);
            return;
          } else {
            commit('isBusy', false);
            commit('errorMessage', {
              title: 'Unable to login!',
              text: 'Please check your username or password.',
              timeout: 5000,
            });
            reject(new Error('User not found in database'));
          }
        });
      });
    });
  },
  /**
   * Logout of the Firebase auth.
   * @param {Object} vuexParams The provided Vuex params.
   */
  logout({ commit, getters }) {
    commit('isBusy', true);

    return logout(getters.app)
      .then(() => {
        getters.app
          .database()
          .ref(`users/${getters.user.id}`)
          .update({ isActive: false });

        commit('user', null);
      })
      .catch((err) => {
        throw err;
      })
      .finally(() => commit('isBusy', false));
  },
  /**
   * Creates a new account.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {Object} form The registration form.
   */
  async createAccount({ commit, getters }, form) {
    commit('isBusy', true);
    const { username, email, password, avatar } = form;

    try {
      const { uid } = await registerWithEmail(getters.app, email, password);

      const db = getters.app.database();
      db.ref('users')
        .child(uid)
        .set({ username, email, avatar, isActive: true });

      return uid;
    } catch (err) {
      commit('errorMessage', {
        title: 'Unable to create new account.',
        text: err,
        timeout: 5000,
      });
      throw err;
    } finally {
      commit('isBusy', false);
    }
  },
};
