import actions from './account.actions';

const initialState = () => {
  return {
    user: null,
    authState: false,
  };
};

export default {
  state: initialState(),
  mutations: {
    initialState: (state) => Object.assign(state, initialState()),
    user: (state, user) => (state.user = user),
    authState: (state, isAuth) => (state.authState = isAuth),
  },
  getters: {
    user: (state) => state.user,
    authState: (state) => state.authState,
  },
  actions,
};
