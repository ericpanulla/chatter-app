import actions from './app.actions';

const initialState = () => {
  return {
    app: null,
    isBusy: false,
    errorMessage: null,
    title: '',
    subtitle: '',
  };
};

export default {
  state: initialState(),
  mutations: {
    initialState(state) {
      Object.assign(state, initialState());
    },
    app(state, app) {
      state.app = app;
    },
    state(state, initial) {
      Object.assign(state, initial);
    },
    isBusy(state, isBusy) {
      state.isBusy = isBusy;
    },
    errorMessage(state, { title, text, timeout }) {
      state.errorMessage = { title, text };
      setTimeout(() => {
        state.errorMessage = null;
      }, timeout);
    },
    user(state, user) {
      state.user = user;
    },
    title(state, title) {
      state.title = title;
    },
    subtitle(state, subtitle) {
      state.subtitle = subtitle;
    },
  },
  getters: {
    app(state) {
      return state.app;
    },
    isBusy(state) {
      return state.isBusy;
    },
    errorMessage(state) {
      return state.errorMessage;
    },
    title(state) {
      return state.title;
    },
    subtitle(state) {
      return state.subtitle;
    },
  },
  actions,
};
