import config from '../../../../config/firebase-config.json';
import { MODULES, initApp } from '../../../js/firebase/initialize';

export default {
  /**
   * Initializes the app with Firebase.
   * @param {Object} vuexParams The provided Vuex params.
   * @returns {Promise} A promise resolving to the intitialized app, or else rejecting with error.
   */
  initializeApp({ commit }) {
    const { AUTH, DB } = MODULES;

    commit('isBusy', true);
    return new Promise((resolve, reject) => {
      initApp(config, [AUTH, DB])
        .then((app) => {
          commit('app', app);

          console.info('Firebase Initialized.', app);

          resolve(app);
        })
        .catch((err) => {
          console.error('Cannot initialize firebase app.', err);
          console.info('Attempting app reload in... 2 seconds');

          setTimeout(() => {
            commit('isBusy', true);
            location.reload();

            reject(err);
          }, 2000);
        })
        .finally(() => {
          commit('isBusy', false);
        });
    });
  },
  /**
   * Sets the toolbar title.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {*} title The title to set.
   */
  setTitle({ commit }, title) {
    commit('title', title);
  },
  /**
   * Sets the toolbar subtitle.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {*} subtitle The subtitle to set.
   */
  setSubtitle({ commit }, subtitle) {
    commit('subtitle', subtitle);
  },
  /**
   * Registers a Firebase database reference listener for the given user id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} id The user id to watch.
   * @returns {Object} The Firebase database reference.
   */
  onUser({ commit, getters }, id) {
    const ref = getters.app.database().ref(`users/${id}`);
    ref.on('value', (user) => {
      if (user.val()) {
        commit('user', { id: user.key, ...user.val() });
      }
    });

    return ref;
  },
  /**
   * Retrieves the list of users.
   * @param {Object} vuexParams The provided Vuex params.
   * @returns {Promise} A promise resolving the list.
   */
  getUsers({ getters }) {
    return new Promise((resolve) => {
      getters.app
        .database()
        .ref('users')
        .once('value', (users) => {
          const list = Object.keys(users.val()).map((key) => {
            return { id: key, ...users.val()[key] };
          });
          resolve(list);
        });
    });
  },
};
