import actions from './notifications.actions';

const initialState = () => {
  return {
    notifications: [],
  };
};

export default {
  state: initialState(),
  mutations: {
    initialState(state) {
      Object.assign(state, initialState());
    },
    notification(state, notif) {
      state.notifications = [...state.notifications, notif];
    },
    removeNotification(state, notif) {
      const index = state.notifications.findIndex((n) => n.id === notif.id);
      if (index !== -1) {
        const notifs = state.notifications;
        notifs.splice(index, 1);
        state.notifications = notifs;
      }
    },
  },
  getters: {
    notifications(state) {
      return state.notifications;
    },
  },
  actions,
};
