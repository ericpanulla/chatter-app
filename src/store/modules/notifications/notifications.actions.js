export default {
  /**
   * Registers a Firebase database reference listener for notifications of a given user id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} id The user id.
   * @returns {Object} A Firebase database reference.
   */
  onNotifications({ commit, getters }, id) {
    const ref = getters.app.database().ref(`notifications/${id}`);
    ref.on('child_added', (child) => {
      const notif = child.val();
      commit('notification', { id: child.key, ...notif });
    });

    ref.on('child_removed', (child) => {
      setTimeout(() => {
        commit('removeNotification', { id: child.key });
      }, 5000);
    });

    return ref;
  },
  /**
   * Responds to a request.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {OBject} responseParams The response parameters.
   * @param {String} responseParams.userId The id of the user responding.
   * @param {String} responseParams.notifd The id of the notification responding to.
   * @param {Object} responseParams.response The response.
   */
  respondToRequest({ getters }, { userId, notifId, response }) {
    getters.app
      .database()
      .ref(`notifications/${userId}/${notifId}/response`)
      .set(response);
  },
};
