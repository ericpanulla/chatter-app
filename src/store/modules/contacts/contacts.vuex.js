import actions from './contacts.actions';

const initialState = () => {
  return { contacts: {} };
};

export default {
  state: initialState(),
  mutations: {
    initialState(state) {
      Object.assign(state, initialState());
    },
    contact(state, { id, data }) {
      state.contacts = { ...state.contacts, [id]: data };
    },
  },
  getters: {
    contacts(state) {
      return state.contacts;
    },
  },
  actions,
};
