import { mapOnChildAdded } from '../../../js/firebase/utils';

export default {
  /**
   * Registers a Firebase database reference listener for contacts of a given user id.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} id The user id.
   * @returns {Array} An array of Firebase database references.
   */
  onContacts({ commit, getters }, id) {
    return mapOnChildAdded(getters.app.database(), commit, {
      parentId: id,
      parentPath: 'contacts',
      childPath: 'users',
      commitPath: 'contact',
    });
  },
  /**
   * Sends a request for a new contact.
   * @param {Object} vuexParams The provided Vuex params.
   * @param {String} contactId The requested contact id.
   */
  requestContact({ commit, getters }, contactId) {
    const { id, username, avatar } = getters.user;

    const contacts = getters.contacts;
    if (!contacts[contactId]) {
      const notif = {
        type: 'contact',
        data: {
          from: {
            id,
            username,
            avatar,
          },
        },
      };

      getters.app
        .database()
        .ref(`notifications/${contactId}`)
        .push()
        .set(notif);
    } else {
      commit('errorMessage', {
        title: 'Requested user is already a contact!',
        text: 'Please try a different user',
        timeout: 5000,
      });
    }
  },
};
