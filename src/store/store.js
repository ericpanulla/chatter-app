import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import app from '../store/modules/app/app.vuex';
import account from '../store/modules/account/account.vuex';
import chats from '../store/modules/chats/chats.vuex';
import contacts from '../store/modules/contacts/contacts.vuex';
import notifications from '../store/modules/notifications/notifications.vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    account,
    chats,
    contacts,
    notifications,
  },
  plugins: [
    createPersistedState({
      reducer: (persistedState) => {
        const stateFilter = Object.assign({}, persistedState);
        const blackList = ['app', 'authState', 'isBusy'];

        blackList.forEach((item) => {
          delete stateFilter[item];
        });

        return stateFilter;
      },
    }),
  ],
  actions: {
    reset({ commit }) {
      commit('initialState');
    },
  },
});
