import Vue from 'vue';
import './plugins/vuetify';
import App from './components/app/App.vue';
import router from './router';
import store from './store/store';
import vuetify from './plugins/vuetify';

import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.config.productionTip = false;

new Vue({
  router,
  store,

  created() {
    this.$store.dispatch('initializeApp').then(() => {
      this.$store.dispatch('setUserAuth').then(() => {
        if (!this.$store.getters.authState) {
          this.$router.replace({ name: 'login' });
        }
      });
    });
  },

  vuetify,
  render: (h) => h(App),
}).$mount('#app');
