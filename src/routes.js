export default [
  {
    path: '*',
    redirect: { name: 'login' },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('./components/app/AppLoginForm.vue'),
    meta: {
      toolbar: {
        enable: false,
        back: { enable: false },
        logout: { enable: false },
      },
    },
  },
  {
    path: '/chats',
    name: 'chats',
    component: () => import('./components/chat/ChatList.vue'),
    meta: {
      toolbar: {
        enable: true,
        back: { enable: false },
        logout: { enable: true },
      },
    },
  },
  {
    path: '/messages/:id',
    name: 'messages',
    component: () => import('./components/messages/Messages.vue'),
    meta: {
      toolbar: {
        enable: true,
        back: { enable: true, route: 'chats' },
        logout: { enable: true },
      },
    },
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: () => import('./components/contacts/Contacts.vue'),
    meta: {
      toolbar: {
        enable: true,
        back: { enable: true, route: 'chats' },
        logout: { enable: true },
      },
    },
  },
];
