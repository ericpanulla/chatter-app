/**
 * Retrieves the authenticated user from the Firebase App
 * @param {Firebase.App} app The initialized Firebase app.
 */
export const getUser = (app) => {
  return new Promise((resolve, reject) => {
    validateAuthInitialization(app, reject);

    const unsubscribe = app.auth().onAuthStateChanged((user) => {
      unsubscribe();
      resolve(user);
    });
  });
};

/**
 * Login to Firebase with email and password.
 *
 * @param {Firebase.app} app The initialized firebase app. Auth service must be imported.
 * @param {String} email The email to log in.
 * @param {String} password The password to log in.
 * @returns {Promise} A promise resolving to the logged in user, otherwise rejecting to the caused error.
 */
export const loginWithEmail = (app, email, password) => {
  return new Promise((resolve, reject) => {
    validateAuthInitialization(app, reject);

    const unsubscribe = app.auth().onAuthStateChanged((user) => {
      if (user) {
        unsubscribe();
        resolve(user);
      }
    });

    app
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch((err) => {
        unsubscribe();
        reject(err);
      });
  });
};

/**
 * Logout of Firebase.
 *
 * @param {Firebase.app} app The initialized firebase app. Auth service must be imported.
 * @returns {Promise} A promise resolving, otherwise rejecting to the caused error.
 */
export const logout = (app) => {
  return new Promise((resolve, reject) => {
    validateAuthInitialization(app, reject);

    const unsubscribe = app.auth().onAuthStateChanged((user) => {
      if (user === null) {
        unsubscribe();
        resolve();
      }
    });

    app
      .auth()
      .signOut()
      .catch((err) => reject(err));
  });
};

/**
 * Registers a new user to Firebase with email and password.
 *
 * @param {Firebase.app} app The initialized firebase app. Auth service must be imported.
 * @param {String} email The email to log in.
 * @param {String} password The password to log in.
 * @returns {Promise} A promise resolving to the logged in user, otherwise rejecting to the caused error.
 */
export const registerWithEmail = (app, email, password) => {
  return new Promise((resolve, reject) => {
    validateAuthInitialization(app, reject);

    app
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        if (response) {
          resolve(response.user);
        }
      })
      .catch((err) => reject(err));
  });
};

/* PRIVATE METHODS */

function validateAuthInitialization(app, reject) {
  if (typeof app.auth !== 'function') {
    reject(new Error('Firebase auth has not been initialized.'));
  }
}
