/**
 * The available Firebase modules.
 */
export const MODULES = {
  AUTH: 'auth',
  DB: 'database',
  STORAGE: 'storage',
};

/**
 * Initializes a new Firebase app.
 *
 * @param {Object} config The app config parameters.
 * @param {Array} requiredModules A list of the required modules from export MODULES.
 * @returns {Promise} A promise resolving to the initialized Firebase app.
 */
export const initApp = async (config, requiredModules) => {
  const firebase = await importModules(requiredModules);
  return !firebase.apps.length
    ? firebase.initializeApp(config)
    : firebase.app();
};

/* PRIVATE METHODS */

/**
 * Imports the provided module list dynamically.
 *
 * @param {Array} modules The module list to import.
 * @returns {Promise} A promise resolving after all module imports have finished.
 */
function importModules(modules) {
  return new Promise((resolve, reject) => {
    import('firebase/app')
      .then((firebase) => {
        const importPromises = [];
        modules.map((mod) => {
          importPromises.push(importModule(mod));
        });

        Promise.all(importPromises).then(() => resolve(firebase));
      })
      .catch(() => reject("Unable to import module 'firebase/app'"));
  });
}

/**
 * Imports a module asynchronously based on the provided module name.
 *
 * @param {String} moduleName The module to import.
 * @returns {Promise} A promise resolving when the import is finished.
 */
function importModule(moduleName) {
  switch (moduleName) {
    case MODULES.AUTH:
      return import('firebase/auth');
    case MODULES.DB:
      return import('firebase/database');
    case MODULES.STORAGE:
      return import('firebase/storage');
  }
}
