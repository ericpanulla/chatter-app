/**
 * Maps a Firebase database reference to a Vuex mutation and returns an array of the Firebase references.
 * @param {Firebase.Database} db The Firebase database instance.
 * @param {Function} commit The Vuex mutation commit function passed to a Vuex action.
 * @param {Object} params The database path configuration parameters.
 * @param {String} params.parentId The id of the parent reference.
 * @param {String} params.parentPath The database path of the parent reference.
 * @param {String} params.childPath The database path of the child reference.
 * @param {String} params.commitPath The name of the Vuex mutation to commit mapped values to.
 */
export const mapOnChildAdded = (
  db,
  commit,
  { parentId, parentPath, childPath, commitPath }
) => {
  const refs = [];
  const parentRef = db.ref(`${parentPath}/${parentId}`);
  parentRef.on('child_added', (child) => {
    const childRef = db.ref(`${childPath}/${child.key}`);
    childRef.on('value', (childData) => {
      commit(commitPath, { id: childData.key, data: childData.val() });
    });

    refs.push(childRef);
  });

  refs.push(parentRef);
  return refs;
};
