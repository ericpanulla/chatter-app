const functions = require('firebase-functions');
const db = functions.database;

exports.onMessageRecieved = db
  .ref('/messages/{chatId}/{msgId}')
  .onCreate((snap, context) => {
    const { timestamp, text, from } = snap.val();
    const chatId = context.params.chatId;

    const topRef = snap.ref.parent.parent.parent;
    topRef
      .child(`chats-meta/${chatId}`)
      .update({ lastTimestamp: timestamp, lastMessage: text, from });

    topRef.child(`chats-meta/${chatId}`).once('value', (snap) => {
      if (snap.val()) {
        const userIds = Object.keys(snap.val().userIds).filter(
          (id) => id !== from
        );

        userIds.forEach((id) => {
          topRef
            .child(`notifications/${id}`)
            .push()
            .set({
              type: 'message',
              data: { timestamp, text, from, chatId },
            });
        });

        return true;
      }
    });
  });

exports.onNotifResponse = db
  .ref('/notifications/{toId}/{notifId}')
  .onUpdate((change, context) => {
    const toId = context.params.toId;

    const { type, data, response } = change.after.val();
    if (type === 'contact') {
      const fromId = data.from.id;

      const isAccepted = response && response === 'accepted';
      const isDeclined = response && response === 'declined';

      if (isAccepted) {
        const contactsRef = change.after.ref.parent.parent.parent;
        contactsRef.child(`contacts/${fromId}/${toId}`).set(true);
        contactsRef.child(`contacts/${toId}/${fromId}`).set(true);

        change.after.ref.remove();
        return true;
      } else if (isDeclined) {
        change.after.ref.remove();
        return true;
      }
    }

    if (type === 'message') {
      const isRecieved = response && response === 'recieved';
      if (isRecieved) {
        change.after.ref.remove();
      }
    }
  });
